package di

// Container defines the methods that any immutable container must implement.
type Container interface {
	Resolve(into interface{})
	Fork() Container
}
