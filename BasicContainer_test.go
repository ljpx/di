package di

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestBasicContainerE2E(t *testing.T) {
	// Arrange.
	container := NewContainer()
	container.Register(Singleton, testService1Factory)
	container.Register(Singleton, testService2Factory)

	// Act.
	var ts1 testInterface1
	var ts2 testInterface2
	container.Resolve(&ts1)
	container.Resolve(&ts2)

	// Assert.
	require.Equal(t, "I am testService1", ts1.Announce())
	require.Equal(t, "I am testService2", ts2.Announce())
}

func TestBasicContainerRegisterPanicsWithUnknownLifetime(t *testing.T) {
	// Arrange.
	container := NewContainer()

	// Act and Assert.
	require.Panics(t, func() {
		container.Register(Lifetime(-1), testService1Factory)
	})
}

func TestBasicContainerResolvePanicsWithBadOutParameter1(t *testing.T) {
	// Arrange.
	container := NewContainer()

	// Act and Assert.
	var ts1 testService1
	require.Panics(t, func() {
		container.Resolve(ts1)
	})
}

func TestBasicContainerResolvePanicsWithBadOutParameter2(t *testing.T) {
	// Arrange.
	container := NewContainer()

	// Act and Assert.
	var ts1 *testService1
	require.Panics(t, func() {
		container.Resolve(ts1)
	})
}

func TestBasicContainerResolvePanicsWithNoFactory(t *testing.T) {
	// Arrange.
	container := NewContainer()

	// Act and Assert.
	var ts1 testInterface1
	require.Panics(t, func() {
		container.Resolve(&ts1)
	})
}

func TestBasicContainerRegisterBadFactory1(t *testing.T) {
	// Arrange.
	container := NewContainer()

	// Act and Assert.
	require.Panics(t, func() {
		container.Register(Singleton, func(int) (*testService1, error) {
			return &testService1{}, nil
		})
	})
}

func TestBasicContainerRegisterBadFactory2(t *testing.T) {
	// Arrange.
	container := NewContainer()

	// Act and Assert.
	require.Panics(t, func() {
		container.Register(Singleton, func() testService1 {
			return testService1{}
		})
	})
}

func TestBasicContainerRegisterBadFactory3(t *testing.T) {
	// Arrange.
	container := NewContainer()

	// Act and Assert.
	require.Panics(t, func() {
		container.Register(Singleton, func(int) testInterface1 {
			return &testService1{}
		})
	})
}

func TestBasicContainerInstancePerDependencyMechanics(t *testing.T) {
	// Arrange.
	container := NewContainer()
	container.Register(InstancePerDependency, testService1Factory)

	// Act.
	var ts11 testInterface1
	container.Resolve(&ts11)

	var ts12 testInterface1
	container.Resolve(&ts12)

	// Assert.
	require.Equal(t, "I am testService1", ts11.Announce())
	require.Equal(t, "I am testService1", ts12.Announce())
	require.NotEqual(t, fmt.Sprintf("%p", ts11), fmt.Sprintf("%p", ts12))
}

func TestBasicContainerInstancePerContainerMechanics(t *testing.T) {
	// Arrange.
	container1 := NewContainer()
	container1.Register(InstancePerContainer, testService1Factory)
	container1.Register(Singleton, testService2Factory)

	container2 := container1.Fork()

	// Act.
	var ts11 testInterface1
	container1.Resolve(&ts11)

	var ts12 testInterface1
	container1.Resolve(&ts12)

	var ts13 testInterface1
	container2.Resolve(&ts13)

	var ts21 testInterface2
	container1.Resolve(&ts21)

	var ts22 testInterface2
	container2.Resolve(&ts22)

	// Assert.
	require.Equal(t, "I am testService1", ts11.Announce())
	require.Equal(t, "I am testService1", ts12.Announce())
	require.Equal(t, "I am testService1", ts13.Announce())
	require.Equal(t, "I am testService2", ts21.Announce())
	require.Equal(t, "I am testService2", ts22.Announce())
	require.Equal(t, fmt.Sprintf("%p", ts11), fmt.Sprintf("%p", ts12))
	require.Equal(t, fmt.Sprintf("%p", ts21), fmt.Sprintf("%p", ts22))
	require.NotEqual(t, fmt.Sprintf("%p", ts11), fmt.Sprintf("%p", ts13))
}

func TestBasicContainerSingletonMechanics(t *testing.T) {
	// Arrange.
	container := NewContainer()
	container.Register(Singleton, testService1Factory)

	// Act.
	var ts11 testInterface1
	container.Resolve(&ts11)

	var ts12 testInterface1
	container.Resolve(&ts12)

	// Assert.
	require.Equal(t, "I am testService1", ts11.Announce())
	require.Equal(t, "I am testService1", ts12.Announce())
	require.Equal(t, fmt.Sprintf("%p", ts11), fmt.Sprintf("%p", ts12))
}

func TestBasicContainerMultiLevelResolution(t *testing.T) {
	// Arrange.
	container := NewContainer()
	container.Register(InstancePerDependency, testService3Factory)
	container.Register(InstancePerDependency, testService1Factory)
	container.Register(InstancePerDependency, testService2Factory)

	// Act.
	var ti3 testInterface3
	container.Resolve(&ti3)

	// Assert.
	require.Equal(t, "I am testService1 I am testService2", ti3.Announce())
}

// -----------------------------------------------------------------------------

type announceable interface {
	Announce() string
}

type testInterface1 interface {
	announceable
}
type testInterface2 interface {
	announceable
}

type testInterface3 interface {
	announceable
}

type testService1 struct {
	uniqueForcer int
}

var _ testInterface1 = &testService1{}

func testService1Factory(c Container) testInterface1 {
	return &testService1{}
}

func (*testService1) Announce() string {
	return "I am testService1"
}

type testService2 struct {
	uniqueForcer int
}

var _ testInterface2 = &testService2{}

func testService2Factory(c Container) testInterface2 {
	return &testService2{}
}

func (*testService2) Announce() string {
	return "I am testService2"
}

type testService3 struct {
	ti1 testInterface1
	ti2 testInterface2
}

var _ testInterface3 = &testService3{}

func testService3Factory(c Container) testInterface3 {
	var ti1 testInterface1
	var ti2 testInterface2

	c.Resolve(&ti1)
	c.Resolve(&ti2)

	return &testService3{ti1: ti1, ti2: ti2}
}

func (ts3 *testService3) Announce() string {
	return fmt.Sprintf("%v %v", ts3.ti1.Announce(), ts3.ti2.Announce())
}
