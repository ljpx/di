package di

// MutableContainer defines the methods that any mutable container must
// implement.
type MutableContainer interface {
	Container

	Register(lifetime Lifetime, factoryFunc interface{})
}
