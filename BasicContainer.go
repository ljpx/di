package di

import (
	"fmt"
	"reflect"
	"sync"
)

// BasicContainer is an implementation of MutableContainer.
type BasicContainer struct {
	scopes map[reflect.Type]Scope
	mx     *sync.RWMutex
}

var _ MutableContainer = &BasicContainer{}

// NewContainer returns a new instance of BasicContainer as a MutableContainer.
func NewContainer() MutableContainer {
	return &BasicContainer{
		scopes: make(map[reflect.Type]Scope),
		mx:     &sync.RWMutex{},
	}
}

// Register registers the provided factory function with the given lifetime
// scope.  The factory function must be of the form `func () T` where T is an
// interface.
func (c *BasicContainer) Register(lifetime Lifetime, factoryFunc interface{}) {
	factory, returnType := NewFactory(factoryFunc)
	scope := produceScope(lifetime, factory)

	c.mx.Lock()
	defer c.mx.Unlock()

	c.scopes[returnType] = scope
}

// Resolve resolves into the provided `*T` where T is an interface.
func (c *BasicContainer) Resolve(into interface{}) {
	v := reflect.ValueOf(into)
	t := v.Type()

	if t.Kind() != reflect.Ptr || t.Elem().Kind() != reflect.Interface {
		panicWithBadIntoParameter(t)
	}

	returnType := t.Elem()

	c.mx.RLock()
	defer c.mx.RUnlock()

	scope, ok := c.scopes[returnType]
	if !ok {
		panicWithNoScope(returnType)
	}

	val := scope.GetDependency(c)
	v.Elem().Set(val)
}

// Fork forks the current container.  It ensures lifetime rules around
// singletons are preserved, and creates a new scope for InstancePerContainer
// lifetimes.  InstancePerDependency lifetimes, as usual, will continue to
// create new instances.
func (c *BasicContainer) Fork() Container {
	nc := &BasicContainer{
		scopes: make(map[reflect.Type]Scope),
		mx:     &sync.RWMutex{},
	}

	for returnType, scope := range c.scopes {
		lifetime := scope.GetLifetime()

		if lifetime == InstancePerDependency || lifetime == Singleton {
			nc.scopes[returnType] = scope
		}

		if lifetime == InstancePerContainer {
			nc.scopes[returnType] = NewInstancePerContainerScope(scope.GetFactory())
		}
	}

	return nc
}

func produceScope(lifetime Lifetime, factory *Factory) Scope {
	switch lifetime {
	case InstancePerDependency:
		return NewInstancePerDependencyScope(factory)
	case InstancePerContainer:
		return NewInstancePerContainerScope(factory)
	case Singleton:
		return NewSingletonScope(factory)
	default:
		panicWithUnknownLifetime(lifetime)
		return nil
	}
}

func panicWithUnknownLifetime(lifetime Lifetime) {
	panic(fmt.Sprintf("the provided lifetime '%v' is unknown", lifetime))
}

func panicWithBadIntoParameter(t reflect.Type) {
	panic(fmt.Errorf("the provided `into` parameter is invalid, type must be like `*T`, where T is an interface, but looks more like `%v`", t))
}

func panicWithNoScope(t reflect.Type) {
	panic(fmt.Errorf("there is no scope in the container that can produce values of type `%v`", t))
}
