package di

import "reflect"

// InstancePerDependencyScope defines a scope that always calls the factory to
// produce a new instance each time GetDependency is called.
type InstancePerDependencyScope struct {
	factory *Factory
}

var _ Scope = &InstancePerDependencyScope{}

// NewInstancePerDependencyScope creates a new InstancePerDependencyScope with
// the provided factory.
func NewInstancePerDependencyScope(factory *Factory) *InstancePerDependencyScope {
	return &InstancePerDependencyScope{
		factory: factory,
	}
}

// GetDependency calls the underlying factory function each time it is called.
func (s *InstancePerDependencyScope) GetDependency(c Container) reflect.Value {
	return s.factory.Fabricate(c)
}

// GetFactory returns the factory for this scope.
func (s *InstancePerDependencyScope) GetFactory() *Factory {
	return s.factory
}

// GetLifetime returns the lifetime for this scope.
func (s *InstancePerDependencyScope) GetLifetime() Lifetime {
	return InstancePerDependency
}
