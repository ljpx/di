package di

import (
	"reflect"
	"sync"
)

// SingletonScope defines a scope that, after the first resolution, will
// continue to resolve the same instance each time GetDependency is called.
type SingletonScope struct {
	factory *Factory
	value   *reflect.Value
	mx      *sync.RWMutex
}

var _ Scope = &SingletonScope{}

// NewSingletonScope creates a new SingletonScope with the provided factory.
func NewSingletonScope(factory *Factory) *SingletonScope {
	return &SingletonScope{
		factory: factory,
		value:   nil,
		mx:      &sync.RWMutex{},
	}
}

// GetDependency calls the underlying factory function once, then continues to
// return the same instance on subsequent calls.
func (s *SingletonScope) GetDependency(c Container) reflect.Value {
	value := func() *reflect.Value {
		s.mx.RLock()
		defer s.mx.RUnlock()
		return s.value
	}()

	if value == nil {
		s.performInitialResolution(c)
	}

	return *s.value
}

// GetFactory returns the factory for this scope.
func (s *SingletonScope) GetFactory() *Factory {
	return s.factory
}

// GetLifetime returns the lifetime for this scope.
func (s *SingletonScope) GetLifetime() Lifetime {
	return Singleton
}

func (s *SingletonScope) performInitialResolution(c Container) {
	s.mx.Lock()
	defer s.mx.Unlock()

	if s.value != nil {
		return
	}

	value := s.factory.Fabricate(c)
	s.value = &value
}
