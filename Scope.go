package di

import "reflect"

// Scope defines the methods that any scope must implement.
type Scope interface {
	GetDependency(c Container) reflect.Value
	GetFactory() *Factory
	GetLifetime() Lifetime
}
