package di

import (
	"reflect"
	"sync"
)

// InstancePerContainerScope defines a scope that, after the first resolution,
// will continue to resolve the same instance each time GetDependency is called.
// It is an almost exact duplicate of InstancePerContainerScope.
type InstancePerContainerScope struct {
	factory *Factory
	value   *reflect.Value
	mx      *sync.RWMutex
}

var _ Scope = &InstancePerContainerScope{}

// NewInstancePerContainerScope creates a new InstancePerContainerScope with the
// provided factory.
func NewInstancePerContainerScope(factory *Factory) *InstancePerContainerScope {
	return &InstancePerContainerScope{
		factory: factory,
		value:   nil,
		mx:      &sync.RWMutex{},
	}
}

// GetDependency calls the underlying factory function once, then continues to
// return the same instance on subsequent calls.
func (s *InstancePerContainerScope) GetDependency(c Container) reflect.Value {
	value := func() *reflect.Value {
		s.mx.RLock()
		defer s.mx.RUnlock()
		return s.value
	}()

	if value == nil {
		s.performInitialResolution(c)
	}

	return *s.value
}

// GetFactory returns the factory for this scope.
func (s *InstancePerContainerScope) GetFactory() *Factory {
	return s.factory
}

// GetLifetime returns the lifetime for this scope.
func (s *InstancePerContainerScope) GetLifetime() Lifetime {
	return InstancePerContainer
}

func (s *InstancePerContainerScope) performInitialResolution(c Container) {
	s.mx.Lock()
	defer s.mx.Unlock()

	if s.value != nil {
		return
	}

	value := s.factory.Fabricate(c)
	s.value = &value
}
