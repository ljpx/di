package di

// Lifetime is an enum type that defines the three available lifetime
// scopes.
//
// InstancePerDependency resolves a new instance every time.
//
// InstancePerContainer resolves the same instance each time within the scope of
// a single container.
//
// Singleton will always resolve the same instance.
type Lifetime int

// The collection of available Lifetimes.
const (
	InstancePerDependency Lifetime = 0
	InstancePerContainer  Lifetime = 1
	Singleton             Lifetime = 2
)
