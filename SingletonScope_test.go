package di

import (
	"fmt"
	"reflect"
	"sync"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestSingletonScopeSameInstanceEachTimeEvenAcrossThreads(t *testing.T) {
	// Arrange.
	instanceNumber := 0
	factoryFunc := func(c Container) singletonScopeTestInterface {
		instanceNumber++
		return &singletonScopeTestStruct{
			instanceNumber: instanceNumber,
		}
	}

	factory, _ := NewFactory(factoryFunc)
	scope := NewSingletonScope(factory)

	wg := &sync.WaitGroup{}
	dc := make(chan reflect.Value)
	for i := 0; i < 5; i++ {
		wg.Add(1)

		go func() {
			dc <- scope.GetDependency(NewContainer())
			wg.Done()
		}()
	}

	go func() {
		wg.Wait()
		close(dc)
	}()

	// Act and Assert.
	for val := range dc {
		inst, ok := val.Interface().(*singletonScopeTestStruct)
		require.True(t, ok)
		require.Equal(t, "1", inst.OutputInstanceNumber())
	}
}

func TestSingletonScopeGetFactory(t *testing.T) {
	// Arrange.
	factoryFunc := func(c Container) instancePerDependencyScopeTestInterface {
		return &instancePerDependencyScopeTestStruct{}
	}
	factory1, _ := NewFactory(factoryFunc)
	scope := NewSingletonScope(factory1)

	// Act.
	factory2 := scope.GetFactory()

	// Assert.
	require.Exactly(t, factory1, factory2)
}

func TestSingletonScopeGetLifetime(t *testing.T) {
	// Arrange, Act and Assert.
	require.Equal(t, Singleton, (&SingletonScope{}).GetLifetime())
}

// -----------------------------------------------------------------------------

type singletonScopeTestInterface interface {
	OutputInstanceNumber() string
}

type singletonScopeTestStruct struct {
	instanceNumber int
}

var _ singletonScopeTestInterface = &singletonScopeTestStruct{}

func (ts *singletonScopeTestStruct) OutputInstanceNumber() string {
	return fmt.Sprintf("%v", ts.instanceNumber)
}
