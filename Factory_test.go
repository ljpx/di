package di

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestFactoryNotFunc(t *testing.T) {
	// Arrange.
	factoryFunc := 42

	// Act and Assert.
	expectedPanicMsg := "the provided factory function is invalid, type must be like `func (di.Container) T`, where T is an interface, but looks more like `int`"
	require.PanicsWithValue(t, expectedPanicMsg, func() {
		NewFactory(factoryFunc)
	})
}

func TestFactoryNoReturnType(t *testing.T) {
	// Arrange.
	factoryFunc := func() {}

	// Act and Assert.
	expectedPanicMsg := "the provided factory function is invalid, type must be like `func (di.Container) T`, where T is an interface, but looks more like `func()`"
	require.PanicsWithValue(t, expectedPanicMsg, func() {
		NewFactory(factoryFunc)
	})
}

func TestFactoryMultipleReturnTypes(t *testing.T) {
	// Arrange.
	factoryFunc := func() (int, int) { return 0, 1 }

	// Act and Assert.
	expectedPanicMsg := "the provided factory function is invalid, type must be like `func (di.Container) T`, where T is an interface, but looks more like `func() (int, int)`"
	require.PanicsWithValue(t, expectedPanicMsg, func() {
		NewFactory(factoryFunc)
	})
}

func TestFactoryMoreThanZeroInputs(t *testing.T) {
	// Arrange.
	factoryFunc := func(x int) int { return x }

	// Act and Assert.
	expectedPanicMsg := "the provided factory function is invalid, type must be like `func (di.Container) T`, where T is an interface, but looks more like `func(int) int`"
	require.PanicsWithValue(t, expectedPanicMsg, func() {
		NewFactory(factoryFunc)
	})
}

func TestFactoryReturnTypeNotInterface(t *testing.T) {
	// Arrange.
	factoryFunc := func() int { return 0 }

	// Act and Assert.
	expectedPanicMsg := "the provided factory function is invalid, type must be like `func (di.Container) T`, where T is an interface, but looks more like `func() int`"
	require.PanicsWithValue(t, expectedPanicMsg, func() {
		NewFactory(factoryFunc)
	})
}

func TestFactorySuccess(t *testing.T) {
	// Arrange.
	factoryFunc := func(c Container) factoryTestInterface { return &factoryTestStruct{} }

	// Act.
	factory, returnType := NewFactory(factoryFunc)
	val, ok := factory.Fabricate(NewContainer()).Interface().(*factoryTestStruct)

	// Assert.
	require.True(t, ok)
	require.Equal(t, "Hello, World!", val.Output())
	require.Equal(t, "di.factoryTestInterface", returnType.String())
}

// -----------------------------------------------------------------------------

type factoryTestInterface interface {
	Output() string
}

type factoryTestStruct struct{}

var _ factoryTestInterface = &factoryTestStruct{}

func (*factoryTestStruct) Output() string {
	return "Hello, World!"
}
