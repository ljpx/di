package di

import (
	"fmt"
	"reflect"
	"sync"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestInstancePerContainerScopeSameInstanceEachTimeEvenAcrossThreads(t *testing.T) {
	// Arrange.
	instanceNumber := 0
	factoryFunc := func(c Container) instancePerContainerScopeTestInterface {
		instanceNumber++
		return &instancePerContainerScopeTestStruct{
			instanceNumber: instanceNumber,
		}
	}

	factory, _ := NewFactory(factoryFunc)
	scope := NewInstancePerContainerScope(factory)

	wg := &sync.WaitGroup{}
	dc := make(chan reflect.Value)
	for i := 0; i < 5; i++ {
		wg.Add(1)

		go func() {
			dc <- scope.GetDependency(NewContainer())
			wg.Done()
		}()
	}

	go func() {
		wg.Wait()
		close(dc)
	}()

	// Act and Assert.
	for val := range dc {
		inst, ok := val.Interface().(*instancePerContainerScopeTestStruct)
		require.True(t, ok)
		require.Equal(t, "1", inst.OutputInstanceNumber())
	}
}

func TestInstancePerContainerScopeGetFactory(t *testing.T) {
	// Arrange.
	factoryFunc := func(c Container) instancePerDependencyScopeTestInterface {
		return &instancePerDependencyScopeTestStruct{}
	}
	factory1, _ := NewFactory(factoryFunc)
	scope := NewInstancePerContainerScope(factory1)

	// Act.
	factory2 := scope.GetFactory()

	// Assert.
	require.Exactly(t, factory1, factory2)
}

func TestInstancePerContainerScopeGetLifetime(t *testing.T) {
	// Arrange, Act and Assert.
	require.Equal(t, InstancePerContainer, (&InstancePerContainerScope{}).GetLifetime())
}

// -----------------------------------------------------------------------------

type instancePerContainerScopeTestInterface interface {
	OutputInstanceNumber() string
}

type instancePerContainerScopeTestStruct struct {
	instanceNumber int
}

var _ instancePerContainerScopeTestInterface = &instancePerContainerScopeTestStruct{}

func (ts *instancePerContainerScopeTestStruct) OutputInstanceNumber() string {
	return fmt.Sprintf("%v", ts.instanceNumber)
}
