package di

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestInstancePerDependencyScopeDifferentInstanceEachTime(t *testing.T) {
	// Arrange.
	instanceNumber := 0
	factoryFunc := func(c Container) instancePerDependencyScopeTestInterface {
		instanceNumber++
		return &instancePerDependencyScopeTestStruct{
			instanceNumber: instanceNumber,
		}
	}

	factory, _ := NewFactory(factoryFunc)
	scope := NewInstancePerDependencyScope(factory)

	// Act.
	inst1, ok1 := scope.GetDependency(NewContainer()).Interface().(*instancePerDependencyScopeTestStruct)
	inst2, ok2 := scope.GetDependency(NewContainer()).Interface().(*instancePerDependencyScopeTestStruct)

	// Assert.
	require.True(t, ok1)
	require.True(t, ok2)
	require.Equal(t, "1", inst1.OutputInstanceNumber())
	require.Equal(t, "2", inst2.OutputInstanceNumber())
}

func TestInstancePerDependencyScopeGetFactory(t *testing.T) {
	// Arrange.
	factoryFunc := func(c Container) instancePerDependencyScopeTestInterface {
		return &instancePerDependencyScopeTestStruct{}
	}
	factory1, _ := NewFactory(factoryFunc)
	scope := NewInstancePerDependencyScope(factory1)

	// Act.
	factory2 := scope.GetFactory()

	// Assert.
	require.Exactly(t, factory1, factory2)
}

func TestInstancePerDependencyScopeGetLifetime(t *testing.T) {
	// Arrange, Act and Assert.
	require.Equal(t, InstancePerDependency, (&InstancePerDependencyScope{}).GetLifetime())
}

// -----------------------------------------------------------------------------

type instancePerDependencyScopeTestInterface interface {
	OutputInstanceNumber() string
}

type instancePerDependencyScopeTestStruct struct {
	instanceNumber int
}

var _ instancePerDependencyScopeTestInterface = &instancePerDependencyScopeTestStruct{}

func (ts *instancePerDependencyScopeTestStruct) OutputInstanceNumber() string {
	return fmt.Sprintf("%v", ts.instanceNumber)
}
