![](icon.png)

# di

Package `di` defines a dependency-injection container with support for different
lifetimes.

## Usage Example

To start, we create a `di.MutableContainer` by calling:

```go
c := di.NewContainer()
```

We can now register dependencies into the container.  We do this by defining
a function similar to the one below:

```go
func (c di.Container) PhysicalDistanceCalculator {
    ...
    return &HaversineCalculator{}
}
```

This is called a factory function.  The return type _must_ be an interface. We
can register this factory function into the container like so:

```go
c.Register(di.InstancePerDependency, func (c di.Container) PhysicalDistanceCalculator {
    return &HaversineCalculator{}
})
```

Once we are finished registering our dependencies, we can pass the container to
other areas of our application that can then resolve dependencies:

```go
func calculateDistanceEndpoint(c di.Container) http.HandlerFunc {
    return func (w http.ResponseWriter, r *http.Request) {
        var distanceCalculator PhysicalDistanceCalculator
        c.Resolve(&distanceCalculator)

        ...
    }
}
```

The factory function expects an input parameter of type `di.Container` - this is
a container you can resolve from for your dependencies:

```go
c.Register(di.Singleton, func (c di.Container) ProximityCalculator {
    var distanceCalculator PhysicalDistanceCalculator
    c.Resolve(&distanceCalculator)

    return &LinearProximityCalculator{distanceCalculator: distanceCalculator}
})
```

## Lifetimes

In the example above, we registered our factory function with a lifetime:
`InstancePerDependency`.  Lifetimes control the "life" of an instance - does it
exist for just this single call to `Resolve` or do we always resolve the same
instance?  The available lifetimes are detailed below:

- `InstancePerDependency` - Every call to `Resolve` will call the factory
function again.  That is, you are guaranteed a new instance every time (with one
caveat, detailed in the next section).

- `InstancePerContainer` - Acts like `Singleton` below, but only in the scope of
a single container.  Calling `Fork` on a container will create a new scope for
`InstancePerContainer` registrations.  Any other registrations will continue to
behave the same way.

- `Singleton` - The first call to `Resolve` calls the factory function.  Any
further calls return the same instance that was returned from the initial call.

## Lifetime Caveat

In Go, types and structs that contain no elements are implicitly singletons.
Creating a type like the following:

```go
type emptyType struct{}
```

... and then registering it as `InstancePerDependency` will not achieve the
expected result.  The factory function will be called each time, but a pointer
to the same instance will be returned.  This is an optimization by the Go
compiler.  There are no real-world implications of this.
