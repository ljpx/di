package di

import (
	"fmt"
	"reflect"
)

// Factory wraps a factory function.  Creating a new factory with NewFactory
// will ensure the validity of the passed interface as a well-defined factory
// function.
type Factory struct {
	factoryFunc reflect.Value
}

var containerReflectionType = reflect.TypeOf((*Container)(nil)).Elem()

// NewFactory creates a new factory using the passed interface{}, expecting it
// to be a function of the form `func (c di.Container) T` where T is an
// interface.
func NewFactory(factoryFunc interface{}) (*Factory, reflect.Type) {
	v := reflect.ValueOf(factoryFunc)
	t := v.Type()

	if t.Kind() != reflect.Func || t.NumOut() != 1 || t.NumIn() != 1 {
		panicWithBadFactory(t)
	}

	if t.Out(0).Kind() != reflect.Interface {
		panicWithBadFactory(t)
	}

	if t.In(0) != containerReflectionType {
		panicWithBadFactory(t)
	}

	return &Factory{
		factoryFunc: v,
	}, t.Out(0)
}

// Fabricate calls the factory function to produce a new instance.
func (f *Factory) Fabricate(c Container) reflect.Value {
	return f.factoryFunc.Call([]reflect.Value{reflect.ValueOf(c)})[0]
}

func panicWithBadFactory(t reflect.Type) {
	panic(fmt.Sprintf("the provided factory function is invalid, type must be like `func (di.Container) T`, where T is an interface, but looks more like `%v`", t))
}
